package com.codercampus.api.error.ennum;

public enum EErrorType {
    WRONG_DATA_FORMAT,
    RESOURCE_NOT_FOUND,
    INVALID_DATA,
    INVALID_RESOURCE_ID,
    RESOURCE_NOT_UPDATED,
    RESOURCE_NOT_CREATED,
    RESOURCE_ALREADY_EXISTS

}
